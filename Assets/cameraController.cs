﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraController : MonoBehaviour {

   // private Vector3 position;
    public Camera mainCamera;

    public Transform player1, player2;
    public float minSizeY = 5f;
    public float maxSizeY = 10f;
    float width;
    float height;

    void SetCameraPos()
    {
        Vector3 middle = (player1.position + player2.position) * 0.5f;
        if(player1.gameObject.activeInHierarchy == false)
        {
            middle = player2.position;
        }
        else if(player2.gameObject.activeInHierarchy == false)
        {
            middle = player1.position;
        }
        GetComponent<Camera>().transform.position = new Vector3(
            middle.x,
            0f,
            GetComponent<Camera>().transform.position.z
        );
    }

    void SetCameraSize()
    {
        //horizontal size is based on actual screen ratio
        float minSizeX = minSizeY * Screen.width / Screen.height;

        //multiplying by 0.5, because the ortographicSize is actually half the height
        if (player1.gameObject.activeInHierarchy == true && player2.gameObject.activeInHierarchy == true)
        {
            float width = Mathf.Abs(player1.position.x - player2.position.x) * 0.5f;
            float height = Mathf.Abs(player1.position.y - player2.position.y) * 0.5f;
        }
        else if (player1.gameObject.activeInHierarchy == false)
        {
            float width = Mathf.Abs(player2.position.x) * 0.5f;
            float height = Mathf.Abs(player2.position.y) * 0.5f;
        }
        else if (player2.gameObject.activeInHierarchy == false)
        {
            float width = Mathf.Abs(player1.position.x) * 0.5f;
            float height = Mathf.Abs(player1.position.y) * 0.5f;
        }

        //computing the size
        float camSizeX = Mathf.Max(width, minSizeX);
        GetComponent<Camera>().orthographicSize = Mathf.Clamp(Mathf.Max(height, camSizeX * Screen.height / Screen.width, minSizeY), minSizeY, maxSizeY);
    }

    // Update is called once per frame
	void Update () {
        SetCameraPos();
        SetCameraSize();
        //       position = new Vector3(player.transform.position.x, player.transform.position.y, -20.0f);
        //        gameObject.transform.position = position;
        
        
    }
}
