﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public float duration = 2f;

    public Vector3 velocity = Vector3.zero;
    public GameObject myCreator;
    public GameObject gameControl;
   // public int Speed = 2;


	// Use this for initialization
	void Start () {
        gameControl = GameObject.FindGameObjectWithTag("GameController");
	}
	
	// Update is called once per frame
	void Update () {
        duration -= Time.deltaTime;
        if (duration <= 0f)
            Destroy(gameObject);

        transform.position += velocity * Time.deltaTime;
	}


    void OnTriggerEnter(Collider other)
    {
        if ( myCreator.tag == "Player" && other.tag == "Enemy")
        {
            other.gameObject.SetActive(false);
            gameControl.GetComponent<gameController>().enemiesToRespawn.Add(other.gameObject);
            Destroy(gameObject);
        }
        else if (myCreator.tag == "Enemy" && other.tag == "Player")
        {
            other.gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else if (myCreator.tag == "Player" && other.tag == "Player")
        {
            return;
        }
        else if (myCreator.tag == "Enemy" && other.tag == "Enemy")
        {
            return;
        }
        else if (other.tag == "Bullet")
        {
            return;
        }
        else if (other.tag == "Checkpoint" || other.tag == "Respawn") {
            return;
        }
        else
        {
            Destroy(gameObject);
        }
    }


    public void SetVelocity(Vector3 v)
    {
        velocity = v;
    }
}
