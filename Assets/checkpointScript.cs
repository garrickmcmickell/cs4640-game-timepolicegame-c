﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class checkpointScript : MonoBehaviour {

    public Vector3 location;
    public float addTime = 0.0f;
    public gameController gameController;
    public replayController replayController;
    public bool isRespawning;
    public bool used;

	// Use this for initialization
	void Start () {
        location = transform.position;
        used = false;

        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<gameController>();
        replayController = GameObject.FindGameObjectWithTag("ReplayController").GetComponent<replayController>();
    }
	
    void OnTriggerEnter(Collider other) {
        if(other.tag == "Player" && !used) {
            used = true;

            gameController.currentCheckpoint = this;
            gameController.timerTime += addTime;

            if (!isRespawning) {
                replayController.ResetReplay();

                for (int i = 0; i < gameController.enemiesToRespawn.Count; i++) {
                    Destroy(gameController.enemiesToRespawn[i]);
                }

                gameController.enemiesToRespawn.Clear();
            }
        }
    }
}
