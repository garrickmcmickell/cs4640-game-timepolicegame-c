﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class replayController : MonoBehaviour {

    public int replayRate = 10;
    public int replayCounter = 0;
    public int currentReplayFrame = 0;
    public Image replayWindow;
    public Text timerText;
    public Camera mainCamera;
    public Material material;
    public bool isReplaying;
    public List<Texture2D> replayImages;
    public gameController gameController;

    // Use this for initialization
    void Start() {
        isReplaying = false;
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<gameController>();
    }

    // Update is called once per frame
    void Update() {
        
    }

    void FixedUpdate() {
        if (!isReplaying) {
            StartCoroutine(SaveFrame());
        }
        else if(isReplaying) {
            StopCoroutine(SaveFrame());
            PlayReplay();
        }
    }

    public IEnumerator SaveFrame() {
        yield return new WaitForEndOfFrame();

        replayCounter++;

        if (replayCounter % replayRate == 0) {
            Texture2D currentFrame = new Texture2D(Screen.width, Screen.height, TextureFormat.RGBA32, true);
            currentFrame.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
            currentFrame.Apply();
            replayImages.Add(currentFrame);
            currentReplayFrame++;
        }
    }

    public void ResetReplay() {
        currentReplayFrame = 0;
        replayImages.Clear();     
    }

    private void PlayReplay() {
        isReplaying = true;
        mainCamera.orthographic = false;
        timerText.enabled = false;
        replayWindow.enabled = true;

        if (currentReplayFrame > 0) {
            replayWindow.canvasRenderer.SetMaterial(material, replayImages[currentReplayFrame - 1]);
            currentReplayFrame--;
        }
        else {
            isReplaying = false;
            mainCamera.orthographic = true;
            timerText.enabled = true;
            replayImages.Clear();
            replayWindow.enabled = false;

            gameController.EnableMovement();
        }
    }
}
