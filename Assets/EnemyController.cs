﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    private float attackCooldown = 0f;
    public float bulletDelay = 1f;
    public bool isDead = false;
    public bool isFacingLeft;
    public bool isFacingRight;
    public bool isCrouched;
    public bool isReplaying = false;
    private Vector3 FacingDirection;
    Rigidbody enemyRigidbody;
    public Material standingFrame;
    public Material standingLeftFrame;
    public Material crouchingFrame;
    public Material crouchingLeftFrame;
    public Transform[] shootingSpawnPoints;
    public GameObject bulletPrefab;
    private Transform currentShootingSpawnPoint;
    public float bulletSpeed = 16f;

    void Awake()
    {
        enemyRigidbody = GetComponent<Rigidbody>();
        currentShootingSpawnPoint = shootingSpawnPoints[0];
    }

    // Use this for initialization
    void Start()
    {
        DetermineSprite();
    }

    // Update is called once per frame
    void Update() {
        if (!isReplaying) {
            attackCooldown -= Time.deltaTime;
            if (attackCooldown <= 0f) {
                DoAttack();
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Player")
        {
            Debug.Log("Enemy hit player");
            other.gameObject.SetActive(false);
        }
        else
        {
            return;
        }
    }

    public void DetermineSprite()
    {
        if (isFacingRight && isCrouched)
        {
            gameObject.transform.localScale = new Vector3(3.0f, 1.3333f, 1.0f);
            gameObject.transform.localPosition -= new Vector3(0.0f, 1.0f, 0.0f);
            GetComponent<Renderer>().material = crouchingFrame;
        }
        else if (isFacingRight && !isCrouched)
        {
            GetComponent<Renderer>().material = standingFrame;
        }
        else if (isFacingLeft && isCrouched)
        {
            gameObject.transform.localScale = new Vector3(3.0f, 1.3333f, 1.0f);
            gameObject.transform.localPosition -= new Vector3(0.0f, 1.0f, 0.0f);
            GetComponent<Renderer>().material = crouchingLeftFrame;
        }
        else if (isFacingLeft && !isCrouched)
        {
            GetComponent<Renderer>().material = standingLeftFrame;
        }

    }
    public void DoAttack()
    {
        if (isFacingLeft)
        {
            FacingDirection = -transform.right;

            if (isCrouched)
            {
                currentShootingSpawnPoint = shootingSpawnPoints[3];
            }
            else
            {
                currentShootingSpawnPoint = shootingSpawnPoints[2];
            }
        }

        else if (isFacingRight)
        {
            FacingDirection = transform.right;

            if (isCrouched)
            {
                currentShootingSpawnPoint = shootingSpawnPoints[1];
            }
            else
            {
                currentShootingSpawnPoint = shootingSpawnPoints[0];
            }
        }
            SpawnBullet(currentShootingSpawnPoint.position, FacingDirection, bulletSpeed);
            attackCooldown = bulletDelay;
    }

    private void SpawnBullet(Vector3 position, Vector3 direction, float speed)
    {
        GameObject bObj = Instantiate(bulletPrefab, position, Quaternion.identity);
        Bullet b = bObj.GetComponent<Bullet>();

        b.myCreator = gameObject;
        b.SetVelocity(direction * speed);
    }

}
