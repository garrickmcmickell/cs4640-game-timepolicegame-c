﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gameController : MonoBehaviour {

    public float timerTime = 0.0f;
    public Text timerText;
    public GameObject player;
    public GameObject player2;
    public GameObject replayController;
    public GameObject cameraController;
    public checkpointScript currentCheckpoint;
    public List<GameObject> enemiesToRespawn;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        if (replayController.GetComponent<replayController>().isReplaying == false) {
            timerTime -= Time.deltaTime;
            timerText.text = timerTime.ToString("n2");

            if (timerTime <= 0) {
                replayController.GetComponent<replayController>().isReplaying = true;               
                Respawn();
            }
        }

        if(player.gameObject.activeInHierarchy == false && player2.gameObject.activeInHierarchy == false) {
            timerTime = 0.0f;
            replayController.GetComponent<replayController>().isReplaying = true;
            Respawn();
        }
	}

    void FixedUpdate() {

    }

    public void Respawn() {
        currentCheckpoint.used = false;
        currentCheckpoint.isRespawning = true;
        player.GetComponent<playerController>().isReplaying = true;
        player2.GetComponent<playerController>().isReplaying = true;
        player.transform.position = new Vector3(currentCheckpoint.location.x + 1.5f, currentCheckpoint.location.y, 0);
        player2.transform.position = new Vector3(currentCheckpoint.location.x - 1.5f, currentCheckpoint.location.y, 0);
        player.GetComponent<playerController>().Reset();
        player2.GetComponent<playerController>().Reset();

        foreach(GameObject e in enemiesToRespawn) {
            e.SetActive(true);
            e.GetComponent<EnemyController>().isReplaying = true;
        }
    }

    public void EnableMovement() {
        foreach (GameObject e in enemiesToRespawn) {
            e.GetComponent<EnemyController>().isReplaying = false;
        }

        enemiesToRespawn.Clear();

        player.GetComponent<playerController>().isReplaying = false;
        player2.GetComponent<playerController>().isReplaying = false;
    }
}
