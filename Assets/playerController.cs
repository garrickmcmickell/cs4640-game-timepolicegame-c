﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour {

    public float jumpForce = 5.0f;
    public float runningSpeed = 0.2f;
    public float rollingSpeed = 0.2f;
    public float attackCooldown = 0f;
    public float bulletSpeed = 16f;
    public Material standingFrame;
    public Material standingLeftFrame;
    public Material crouchingFrame;
    public Material crouchingLeftFrame;
    public Material[] runningRightFrames;
    public Material[] runningLeftFrames;
    public Material[] rollingRightFrames;
    public Material[] rollingLeftFrames;
    public Transform[] shootingSpawnPoints;

    public GameObject bulletPrefab;
    public bool isDead = false;
    public bool isReplaying = false;

    private bool isJumping;
    private bool isSpinningRight;
    private bool isSpinningLeft;
    private bool isCrouched;
    private bool isRunningRight;
    private bool isRollingRight;
    private bool isFacingRight;
    private bool isRunningLeft;
    private bool isRollingLeft;
    private bool isFacingLeft;
    private bool isFalling;
    private bool isLookingup;
    private float runningTimer;
    private Rigidbody rb;
    private Vector3 FacingDirection;
    private Transform currentShootingSpawnPoint;

    private float standingHeight;

    //For xbox controllers
    public string JumpButton = "Jump_P1";
    public string HorizontalControl = "Horizontal_P1";
    public string VerticalControl = "Vertical_P1";
    public string FireButton = "Fire1_P1";

    // Use this for initialization

    void Start () {
        isJumping = false;
        isSpinningRight = false;
        isSpinningLeft = false;
        isCrouched = false;
        isRunningRight = false;
        isRollingRight = false;
        isFacingRight = true;
        isRunningLeft = false;
        isRollingLeft = false;
        isFacingLeft = false;
        isFalling = false;
        isLookingup = false;
        currentShootingSpawnPoint = shootingSpawnPoints[0];
        float standingHeight = GetComponent<Collider>().bounds.size.y;

    rb = GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update () {
	    attackCooldown -= Time.deltaTime;
        float h = Input.GetAxis(HorizontalControl);
        float v = Input.GetAxis(VerticalControl);

        if (!isReplaying) {
            CheckDown(v);
            CheckUp(v);
            CheckRight(h);
            CheckLeft(h);

            if ((Input.GetKey(KeyCode.UpArrow) || v < 0) && !isCrouched) {
                isLookingup = true;
            }
            else {
                isLookingup = false;
            }

            if (Input.GetKey(KeyCode.F) || Input.GetButtonDown(FireButton)) {
                if (!isRollingLeft && !isRollingRight) {
                    DoAttack(0.3f);
                }
            }
        }
	}

    void FixedUpdate() {
        CheckJump(); 
    }

    private void CheckDown(float v) {
        if ((Input.GetKey(KeyCode.DownArrow) || v > 0) && !isCrouched && !isRunningRight && !isRunningLeft && !isJumping) {
            gameObject.transform.localScale = new Vector3(3.0f, 1.3333f, 1.0f);
            gameObject.transform.localPosition -= new Vector3(0.0f, 1.0f, 0.0f);

            if (isFacingRight) {
                GetComponent<Renderer>().material = crouchingFrame;
            }

            if (isFacingLeft) {
                GetComponent<Renderer>().material = crouchingLeftFrame;
            }

            isCrouched = true;
        }
    }

    private void CheckUp(float v) {
        if ((Input.GetKey(KeyCode.UpArrow) || v < 0) && isCrouched && !isRollingRight && !isRollingLeft) {
            var crouchingHeight = GetComponent<MeshFilter>().mesh.bounds.extents.y;
            var startPos = transform.position + new Vector3(0, crouchingHeight - (standingHeight * 0.5f), 0);
            var clearance = 0.9f;
            if (!Physics.Raycast(startPos, Vector3.up, clearance))
            {
                isCrouched = false;

                gameObject.transform.localScale = new Vector3(2.0f, 3.0f, 1.0f);
                gameObject.transform.localPosition += new Vector3(0.0f, 0.6666f, 0.0f);

                if (isFacingRight)
                {
                    GetComponent<Renderer>().material = standingFrame;
                }

                if (isFacingLeft)
                {
                    GetComponent<Renderer>().material = standingLeftFrame;
                }
            }
            else
            {
                isCrouched = true;
            }
        }
    }

    private void CheckRight(float h) {
        if ((Input.GetKey(KeyCode.RightArrow) || h > 0) && !Input.GetKey(KeyCode.LeftArrow) && !isCrouched && !isJumping) {
            isFacingRight = true;
            isFacingLeft = false;

            if (isRunningRight == false) {
                isRunningRight = true;
                runningTimer = 0.0f;
            }

            runningTimer += Time.deltaTime;

            if (runningTimer >= 0 && runningTimer <= 0.05f) {
                GetComponent<Renderer>().material = runningRightFrames[0];
            }
            else if (runningTimer > 0.05f && runningTimer <= 0.1f) {
                GetComponent<Renderer>().material = runningRightFrames[1];
            }
            else if (runningTimer > 0.1f && runningTimer <= 0.15f) {
                GetComponent<Renderer>().material = runningRightFrames[2];
            }
            else if (runningTimer > 0.15f && runningTimer <= 0.2f) {
                GetComponent<Renderer>().material = runningRightFrames[3];
            }
            else if (runningTimer > 0.2f && runningTimer <= 0.25f) {
                GetComponent<Renderer>().material = runningRightFrames[4];
            }
            else {
                runningTimer = 0.0f;
            }

            gameObject.transform.localPosition += new Vector3(runningSpeed, 0.0f, 0.0f);

        }
        else if ((Input.GetKey(KeyCode.RightArrow) || h > 0) && !Input.GetKey(KeyCode.LeftArrow) && isCrouched && isFacingRight && !isJumping) {
            if (isRollingRight == false) {
                gameObject.transform.localScale = new Vector3(2.0f, 2.0f, 1.0f);
                gameObject.transform.localPosition -= new Vector3(0.0f, 0.3333f, 0.0f);

                isRollingRight = true;
                runningTimer = 0.0f;
            }

            runningTimer += Time.deltaTime;

            if (runningTimer >= 0 && runningTimer <= 0.05f) {
                GetComponent<Renderer>().material = rollingRightFrames[0];
            }
            else if (runningTimer > 0.05f && runningTimer <= 0.1f) {
                GetComponent<Renderer>().material = rollingRightFrames[1];
            }
            else if (runningTimer > 0.1f && runningTimer <= 0.15f) {
                GetComponent<Renderer>().material = rollingRightFrames[2];
            }
            else if (runningTimer > 0.15f && runningTimer <= 0.2f) {
                GetComponent<Renderer>().material = rollingRightFrames[3];
            }
            else {
                runningTimer = 0.0f;
            }

            gameObject.transform.localPosition += new Vector3(rollingSpeed, 0.0f, 0.0f);

        }
        else if ((Input.GetKey(KeyCode.RightArrow) || h > 0) && !Input.GetKey(KeyCode.LeftArrow) && isCrouched && isFacingLeft && !isJumping) {
            GetComponent<Renderer>().material = crouchingFrame;

            isFacingLeft = false;
            isFacingRight = true;
        }
        else if ((Input.GetKey(KeyCode.RightArrow) || h > 0) && !Input.GetKey(KeyCode.LeftArrow) && isJumping) {
            isFacingRight = true;
            isFacingLeft = false;

            gameObject.transform.localPosition += new Vector3(rollingSpeed, 0.0f, 0.0f);
        }
        else if (!isJumping) {
            if (isRollingRight) {
                isRollingRight = false;

                gameObject.transform.localScale = new Vector3(3.0f, 1.3333f, 1.0f);
                gameObject.transform.localPosition -= new Vector3(0.0f, 0.3333f, 0.0f);
            }

            isRunningRight = false;

            if (!isRunningLeft && isFacingRight && !isCrouched) {
                GetComponent<Renderer>().material = standingFrame;
            }
            else if (!isRunningLeft && isFacingRight && isCrouched) {
                GetComponent<Renderer>().material = crouchingFrame;
            }
        }
    }

    private void CheckLeft(float h) {
        if ((Input.GetKey(KeyCode.LeftArrow) || h < 0) && !Input.GetKey(KeyCode.RightArrow) && !isCrouched && !isJumping) {
            isFacingLeft = true;
            isFacingRight = false;

            if (isRunningLeft == false) {
                isRunningLeft = true;
                runningTimer = 0.0f;
            }

            runningTimer += Time.deltaTime;

            if (runningTimer >= 0 && runningTimer <= 0.05f) {
                GetComponent<Renderer>().material = runningLeftFrames[0];
            }
            else if (runningTimer > 0.05f && runningTimer <= 0.1f) {
                GetComponent<Renderer>().material = runningLeftFrames[1];
            }
            else if (runningTimer > 0.1f && runningTimer <= 0.15f) {
                GetComponent<Renderer>().material = runningLeftFrames[2];
            }
            else if (runningTimer > 0.15f && runningTimer <= 0.2f) {
                GetComponent<Renderer>().material = runningLeftFrames[3];
            }
            else if (runningTimer > 0.2f && runningTimer <= 0.25f) {
                GetComponent<Renderer>().material = runningLeftFrames[4];
            }
            else {
                runningTimer = 0.0f;
            }

            gameObject.transform.localPosition -= new Vector3(runningSpeed, 0.0f, 0.0f);
        }
        else if ((Input.GetKey(KeyCode.LeftArrow) || h < 0) && !Input.GetKey(KeyCode.RightArrow) && isCrouched && isFacingLeft && !isJumping) {
            if (isRollingLeft == false) {
                gameObject.transform.localScale = new Vector3(2.0f, 2.0f, 1.0f);
                gameObject.transform.localPosition -= new Vector3(0.0f, 0.3333f, 0.0f);

                isRollingLeft = true;
                runningTimer = 0.0f;
            }

            runningTimer += Time.deltaTime;

            if (runningTimer >= 0 && runningTimer <= 0.05f) {
                GetComponent<Renderer>().material = rollingLeftFrames[0];
            }
            else if (runningTimer > 0.05f && runningTimer <= 0.1f) {
                GetComponent<Renderer>().material = rollingLeftFrames[1];
            }
            else if (runningTimer > 0.1f && runningTimer <= 0.15f) {
                GetComponent<Renderer>().material = rollingLeftFrames[2];
            }
            else if (runningTimer > 0.15f && runningTimer <= 0.2f) {
                GetComponent<Renderer>().material = rollingLeftFrames[3];
            }
            else {
                runningTimer = 0.0f;
            }

            gameObject.transform.localPosition -= new Vector3(rollingSpeed, 0.0f, 0.0f);

        }
        else if ((Input.GetKey(KeyCode.LeftArrow) || h < 0) && !Input.GetKey(KeyCode.RightArrow) && isCrouched && isFacingRight && !isJumping) {
            GetComponent<Renderer>().material = crouchingLeftFrame;

            isFacingRight = false;
            isFacingLeft = true;

        }
        else if ((Input.GetKey(KeyCode.LeftArrow) || h < 0) && !Input.GetKey(KeyCode.RightArrow) && isJumping) {
            isFacingRight = false;
            isFacingLeft = true;

            gameObject.transform.localPosition -= new Vector3(rollingSpeed, 0.0f, 0.0f);
        }
        else if (!isJumping) {
            if (isRollingLeft) {
                isRollingLeft = false;

                gameObject.transform.localScale = new Vector3(3.0f, 1.3333f, 1.0f);
                gameObject.transform.localPosition -= new Vector3(0.0f, 0.3333f, 0.0f);
            }

            isRunningLeft = false;

            if (!isRunningRight && isFacingLeft && !isCrouched) {
                GetComponent<Renderer>().material = standingLeftFrame;
            }
            else if (!isRunningRight && isFacingLeft && isCrouched) {
                GetComponent<Renderer>().material = crouchingLeftFrame;
            }
        }
    }

    private void CheckJump() {
        if (rb.velocity.y == 0 && (Input.GetKey(KeyCode.Space) || Input.GetButtonDown(JumpButton)) && !isCrouched) {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);

            isJumping = true;
        }
        else if (rb.velocity.y == 0 && isJumping) {
            gameObject.transform.localScale = new Vector3(2.0f, 3.0f, 1.0f);

            isJumping = false;
            isSpinningLeft = false;
            isSpinningRight = false;
        }
        else if (rb.velocity.y != 0 && isJumping) {
            if (isFacingLeft) {

                if (isSpinningLeft == false) {
                    gameObject.transform.localScale = new Vector3(2.0f, 2.0f, 1.0f);

                    isSpinningLeft = true;
                    runningTimer = 0.0f;
                }

                runningTimer += Time.deltaTime;

                if (runningTimer >= 0 && runningTimer <= 0.05f) {
                    GetComponent<Renderer>().material = rollingLeftFrames[0];
                }
                else if (runningTimer > 0.05f && runningTimer <= 0.1f) {
                    GetComponent<Renderer>().material = rollingLeftFrames[1];
                }
                else if (runningTimer > 0.1f && runningTimer <= 0.15f) {
                    GetComponent<Renderer>().material = rollingLeftFrames[2];
                }
                else if (runningTimer > 0.15f && runningTimer <= 0.2f) {
                    GetComponent<Renderer>().material = rollingLeftFrames[3];
                }
                else {
                    runningTimer = 0.0f;
                }
            }
            else if (isFacingRight) {

                if (isSpinningRight == false) {
                    gameObject.transform.localScale = new Vector3(2.0f, 2.0f, 1.0f);

                    isSpinningRight = true;
                    runningTimer = 0.0f;
                }

                runningTimer += Time.deltaTime;

                if (runningTimer >= 0 && runningTimer <= 0.05f) {
                    GetComponent<Renderer>().material = rollingRightFrames[0];
                }
                else if (runningTimer > 0.05f && runningTimer <= 0.1f) {
                    GetComponent<Renderer>().material = rollingRightFrames[1];
                }
                else if (runningTimer > 0.1f && runningTimer <= 0.15f) {
                    GetComponent<Renderer>().material = rollingRightFrames[2];
                }
                else if (runningTimer > 0.15f && runningTimer <= 0.2f) {
                    GetComponent<Renderer>().material = rollingRightFrames[3];
                }
                else {
                    runningTimer = 0.0f;
                }
            }
        }
    }

    public void DoAttack(float cooldown)
    {
        if (isLookingup)
        {
            FacingDirection = transform.up;
            currentShootingSpawnPoint = shootingSpawnPoints[4];
        }
        else if (isFacingLeft) {
            FacingDirection = -transform.right;

            if (isCrouched) {
                currentShootingSpawnPoint = shootingSpawnPoints[3];
            }
            else {
                currentShootingSpawnPoint = shootingSpawnPoints[2];
            }
        }

        else if (isFacingRight) {
            FacingDirection = transform.right;

            if (isCrouched) {
                currentShootingSpawnPoint = shootingSpawnPoints[1];
            }
            else {
                currentShootingSpawnPoint = shootingSpawnPoints[0];
            }
        }

        if (attackCooldown <= 0f)
        {
            SpawnBullet(currentShootingSpawnPoint.position, FacingDirection, bulletSpeed);
            isLookingup = false;
            attackCooldown = cooldown;
        }
    }

    private void SpawnBullet(Vector3 position, Vector3 direction, float speed)
    {
        GameObject bObj = Instantiate(bulletPrefab, position, Quaternion.identity);
        Bullet b = bObj.GetComponent<Bullet>();

        b.myCreator = gameObject;
        b.SetVelocity(direction * speed);
    }

    public void Reset() {
        isJumping = false;
        isSpinningRight = false;
        isSpinningLeft = false;
        isCrouched = false;
        isRunningRight = false;
        isRollingRight = false;
        isFacingRight = true;
        isRunningLeft = false;
        isRollingLeft = false;
        isFacingLeft = false;
        isFalling = false;

        gameObject.transform.localScale = new Vector3(2.0f, 3.0f, 1.0f);
        gameObject.SetActive(true);
    }
}
